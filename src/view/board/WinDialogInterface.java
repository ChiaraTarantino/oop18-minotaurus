package view.board;

/**
 * 
 * Piero Sanchi. The interface of WinDialog.
 *
 */
public interface WinDialogInterface {

    /**
     * 
     * @param t
     *            the string to write on rank TextArea.
     */
    void setRank(String t);

}
